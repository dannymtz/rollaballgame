﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CountDownTimer : MonoBehaviour {

	float timeRemaining = 30;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timeRemaining -= Time.deltaTime;
	}

	void OnGUI()
	{
		if (timeRemaining > 0) {
			GUI.Label (new Rect (10, 100, 200, 100), "Time: " + timeRemaining);
		}
		else
			SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}
}
